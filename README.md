# Python - Automate displayinh EKS cluster information 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Write a Python script that fetches and displays EKS cluster status and information 

## Technologies Used

* Python 

* Boto3 

* AWS EKS 

## Steps 

Step 1: Create a terraform file to create an EKS cluster

Step 2: Go into terraform directory and perform terraform init this will download all the needed plugins for the providers 

    terraform init 

[Terraform init](/images/01_go_into_terraform_directory_and_perform_terraform_init_this_will_download_all_the_needed_plugins_for_the_providers.png)

[AWS Cluster running](/images/03_aws_cluster_running.png)

Step 3: Create a python file 

[Python file](/images/04_create_a_python_file.png)

Step 4: Import boto3 library into the python file 

[Import boto3](/images/05_import_boto3_library.png)

Step 5: Call the eks boto3 client and save it into a variable 

[eks boto3 client](/images/06_call_the_eks_boto3_client_and_insert_it_to_a_variable.png)

Step 6: Call the function list cluster to display all clusters and save it into a variable 

[list cluster](/images/07_call_the_function_list_cluster_to_display_all_clusters_and_insert_into_variable.png)

[Print clusters](/images/08_print_clusters.png)

Step 7: Write a for loop that loops through every cluster and use the describe cluster function on each 

[loop through cluster](/images/09_write_a_for_loop_that_loops_through_every_cluster_and_use_the_describe_cluster_function_on_each.png)

Step 8: Add logic to for loop to enable it to display lcuster status 

[display clusters status](images/10_add_logic_to_for_loop_to_enable_it_to_display_cluster_status.png)

Step 9: Add logic to for loop to show all endpoints of clusters

[display endpoints](/images/11_add_logic_to_for_loop_to_show_all_endpoints_of_clusters.png)

Step 10: Add logic to loop to display all cluster versions

[display clusters versions](/images/12_add_logic_to_loop_to_display_all_cluster_versions.png)

## Installation

    brew install python3


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-automate-displaying-eks-cluster-information.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-automate-displaying-eks-cluster-information

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.