import boto3

eks_client = boto3.client('eks', region_name="eu-west-2")

clusters = eks_client.list_clusters()['clusters']
for cluster in clusters:
    response = eks_client.describe_cluster(
        name=cluster
    )
    cluster_status = response['cluster']['status']
    cluster_endpoint = response['cluster']['endpoint']
    cluster_version = response['cluster']['version']
    print(f'cluster {cluster} status is {cluster_status}')
    print(f'cluster {cluster}  endpoint is {cluster_endpoint}')
    print(f'cluster {cluster}  version is {cluster_version}')